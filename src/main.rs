use std::io;
use std::time::Duration;

use serial::prelude::*;

use mbee::*;

const DEVICE: &str = "/dev/cu.usbserial-AC012NCO";

fn main() {
    let mut port = serial::open(DEVICE).expect("Open device");
    interact(&mut port).unwrap();
}

fn interact<T: SerialPort>(port: &mut T) -> io::Result<()> {
    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud9600)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowHardware);
        Ok(())
    })?;

    port.set_timeout(Duration::from_secs(60))?;

    let mut buffer = [0u8; 255];

    loop {
        let count = port.read(&mut buffer[..])?;
        println!("read {} bytes", count);

        // let msg = String::from_utf8_lossy(&buffer[..count]);
        // println!("{:?}", msg.trim());
        // let msg = Vec::from(&buffer[..count]);

        match parse_frame(&buffer) {
            Ok(frame) => println!("{:?}", frame),
            Err(err) => eprintln!("Failed to parse frame: {}", err),
        }
    }
}

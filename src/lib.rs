use std::io::{self, Read};
use std::mem;
use std::slice;

use failure::format_err;

const FRAME_START: u8 = 0x7E;

#[repr(C)]
#[derive(Debug)]
pub struct FrameHeader {
    pub start: u8,
    pub length_msb: u8,
    pub length_lsb: u8,
    pub frame_type: u8,
}

#[repr(C)]
#[derive(Debug)]
pub struct IOReceivePacketFrameHeader {
    source_id_msb: u8,
    source_id_lsb: u8,
    pub rssi: u8,
    options: u8,
    temperature: u8,
    pub vbatt: u8,
}

#[derive(Debug, PartialEq)]
pub struct IOReceivePacketFrame {
    source_id: u16,
    rssi: i16,
    options: u8,
    temperature: i16,
    vbatt: f32,
    data: Vec<u8>,
}

impl IOReceivePacketFrame {
    pub fn new(header: IOReceivePacketFrameHeader, data: &[u8]) -> IOReceivePacketFrame {
        let source_id = make_u16(header.source_id_msb, header.source_id_lsb);
        let vbatt = read_voltage(header.vbatt);
        let temperature = read_twos_complement(header.temperature);
        let rssi = read_twos_complement(header.rssi);

        let frame = IOReceivePacketFrame {
            source_id,
            rssi,
            options: header.options,
            temperature,
            vbatt,
            data: Vec::from(data),
        };

        return frame;
    }
}

#[derive(Debug, PartialEq)]
pub enum FrameType {
    Unknown = 0x00,
    TransmitStatus = 0x8B,
    Acknowlege = 0x8C,
    ReceivePacket = 0x81,
    IOReceivePacket = 0x83,
}

impl FrameType {
    pub fn from(code: u8) -> FrameType {
        match code {
            0x8B => FrameType::TransmitStatus,
            0x8C => FrameType::Acknowlege,
            0x81 => FrameType::ReceivePacket,
            0x83 => FrameType::IOReceivePacket,
            _ => FrameType::Unknown,
        }
    }
}

pub fn parse_frame(data: &[u8]) -> Result<IOReceivePacketFrame, failure::Error> {
    if data.is_empty() {
        return Err(format_err!("data must be not empty"));
    }

    println!("data: {:x?}", data);

    if data[0] != FRAME_START {
        return Err(format_err!("frame start not found"));
    }

    if !is_checksum_correct(&data[3..]) {
        return Err(format_err!("frame checksum incorrect"));
    }

    let frame_header = read_struct::<FrameHeader, &[u8]>(data).unwrap();

    let frame_type = FrameType::from(frame_header.frame_type);
    let frame_lenght = make_u16(frame_header.length_msb, frame_header.length_lsb);
    let frame_data = &data[4..(frame_lenght + 3) as usize];

    match frame_type {
        FrameType::IOReceivePacket => {
            let frame_header =
                read_struct::<IOReceivePacketFrameHeader, &[u8]>(&frame_data[..6]).unwrap();

            Ok(IOReceivePacketFrame::new(frame_header, frame_data))
        }
        _ => Err(format_err!("wrong frame type")),
    }
}

#[inline]
fn make_u16(msb: u8, lsb: u8) -> u16 {
    ((msb as u16) << 8) | (lsb as u16)
}

#[inline]
pub fn read_twos_complement(value: u8) -> i16 {
    if value > 128 {
        return -(!(value - 1) as i16);
    }

    return value as i16;
}

pub fn read_voltage(value: u8) -> f32 {
    return ((value as f32 / 51.0) * 100.0).round() / 100.0;
}

pub fn read_struct<T, R: Read>(mut read: R) -> io::Result<T> {
    let size = mem::size_of::<T>();

    unsafe {
        // let mut s = mem::MaybeUninit::uninit();

        let mut s = mem::uninitialized();

        let buffer = slice::from_raw_parts_mut(&mut s as *mut T as *mut u8, size);

        match read.read_exact(buffer) {
            Ok(()) => Ok(s),
            Err(e) => {
                mem::forget(s);
                Err(e)
            }
        }
    }
}

pub fn is_checksum_correct(data: &[u8]) -> bool {
    let mut sum: u32 = 0;

    for byte in data {
        sum += *byte as u32;
    }

    return (sum & 0xFF) == 0xFF;
}

#[test]
fn test_convert_values() {
    let mut temperature = read_twos_complement(0x18);
    assert_eq!(24, temperature);

    temperature = read_twos_complement(0xF9);
    assert_eq!(-7, temperature);

    let voltage = read_voltage(0x98);
    assert_eq!(2.98, voltage);
}

#[test]
fn test_parse_io_receive_packet_frame() {
    // let frame_data1 = [0x7E, 0x00, 0x09, 0x81, 0x00, 0x01, 0xD6, 0x00, 0x00, 0x11, 0x22, 0x33, 0x41];
    let frame_data2 = [
        0x7E, 0x00, 0x21, 0x83, 0x00, 0x01, 0xA5, 0x02, 0x1D, 0x98, 0x04, 0x83, 0x06, 0x0E, 0x00,
        0x00, 0x00, 0x00, 0x09, 0x04, 0x1D, 0x02, 0xFF, 0xFF, 0x1F, 0x04, 0x21, 0x02, 0xFF, 0xFF,
        0x22, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xE7,
    ];
    // let frame_data3 = [0x7E, 0x00, 0x21, 0x83, 0x00, 0x01, 0x9C, 0x02, 0x1D, 0x98, 0x04, 0x83, 0x06, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x09, 0x04, 0x1D, 0x02, 0xFF, 0xFF, 0x1F, 0x04, 0x21, 0x02, 0xFF, 0xFF, 0x22, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xF0];

    let frame = parse_frame(&frame_data2).unwrap();
    println!("{:?}", frame);
}
